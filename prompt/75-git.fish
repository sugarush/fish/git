if test -d .git
  set_color -o magenta
  printf '('
  set git_branch (git symbolic-ref --quiet --short HEAD 2> /dev/null; \
    or git describe --tags --exact-match 2> /dev/null)
  set_color -o normal
  printf '%s' "$git_branch"
  set_color -o magenta
  printf ') '
  set git_status (git status --porcelain 2> /dev/null)
  if test -n "$git_status"
    printf '■ '
  end
  set_color normal
end
