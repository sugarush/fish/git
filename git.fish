if ! test -d $sugar_install_directory/config/prompt
  mkdir -p $sugar_install_directory/config/prompt
end

if ! test -f $sugar_install_directory/config/prompt/75-git.fish
  cp $argv[1]/prompt/75-git.fish $sugar_install_directory/config/prompt
end
